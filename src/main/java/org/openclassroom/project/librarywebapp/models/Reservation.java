package org.openclassroom.project.librarywebapp.models;


import generated.libraryservice.GeneratedReservation;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;


/**
 * Object inherited from {@link GeneratedReservation} allowing a conversion of {@link Date dates} for final posting on
 * the
 * website.
 */
public class Reservation extends GeneratedReservation {
    
    /*
     * =================================================================================================================
     *                                                    ATTRIBUTES
     * =================================================================================================================
     */
    
    private Date reservationConvertedDate;
    
    
    /*
     * =================================================================================================================
     *                                                    CONSTRUCTORS
     * =================================================================================================================
     */
    
    /**
     * Reservation constructor to retrieve {@link GeneratedReservation parent} information and converts {@link Date
     * dates}.
     */
    public Reservation (GeneratedReservation generatedReservation) {
        this.book                     = generatedReservation.getBook();
        this.usager                   = generatedReservation.getUsager();
        this.reservationConvertedDate = toDate(generatedReservation.getReservationDate());
    }
    
    
    /*
     * =================================================================================================================
     *                                                    GETTERS / SETTERS
     * =================================================================================================================
     */
    
    public Date getReservationConvertedDate () {
        return reservationConvertedDate;
    }
    
    public void setReservationDate (Date reservationDate) {
        this.reservationConvertedDate = reservationDate;
    }
    
    /*
     * =================================================================================================================
     *                                                    METHODS
     * =================================================================================================================
     */
    
    /**
     * Converts a date from {@link XMLGregorianCalendar} format to {@link Date} format.
     *
     * @param cal
     *         - The date in {@link XMLGregorianCalendar} format.
     *
     * @return The date in {@link Date} format.
     */
    private Date toDate (XMLGregorianCalendar cal) {
        return cal.toGregorianCalendar().getTime();
    }
    
}
